# Repository comme Jcenter/MavenCentral mais pour les libraries (jar/aar) d'AppOuest.

## Utilisation d'une librairie :

1. Ajouter dans le build.gradle:

```
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath group: 'com.layer', name: 'gradle-git-repo-plugin', version: '2.0.2'
    }
}

apply plugin: 'git-repo'

repositories {
    git("http://sources.appouest.com:10080/appouest-android/appouest-central.git", "com.appouest.test", "master", "releases")
}

```

2. Dans le même fichier au niveau des dépendances :

```
dependencies {
    //...
    compile 'com.appouest:aoutils:0.0.1' //package "com.appouest" + nom librairies + version
}
```

## Ajout d'une librairie dans AppOuestCentral :

1. Cloner tout le dossier
2. Ouvrir AppOuestCentralExamples dans android Studio
3. Créer un nouveau module librairie
4. Ajouter dans le build.gradle :

```
apply plugin: 'maven
uploadArchives {
    repositories.mavenDeployer {
        def deployPath = file(getProperty('aar.deployPath'))
        repository(url: "file://${deployPath.absolutePath}")
        pom.project {
            groupId 'com.appouest'
            artifactId 'NameLibrary' //A modifier
            version "0.0.1" //A modifier
        }
    }
}
```

5. Avant de commit lancer la commande ./gradlew uploadArchives

Elle permet de générer vos artifacts maven.

-> Commit et voilà votre librairie est disponible