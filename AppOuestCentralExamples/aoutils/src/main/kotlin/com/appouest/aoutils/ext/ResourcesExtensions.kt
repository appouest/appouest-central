package com.appouest.aoutils.ext

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat

fun Context.drawable(key: String?): Drawable? {
    if (key == null || key.isEmpty()) {
        return null
    }

    var value: Drawable? = null
    try {
        val imageResource = resources.getIdentifier(key, "drawable", this.packageName)
        value = ContextCompat.getDrawable(this, imageResource)
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return value
}

fun Context.drawable(key: Int): Drawable? {
    var value: Drawable? = null
    try {
        value = ContextCompat.getDrawable(this, key)
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return value
}

fun Context.color(id: Int): Int {
    return ContextCompat.getColor(this, id)
}

fun Context.plurals(pluralsId : Int, quantity : Int) : String? {
    return resources.getQuantityString(pluralsId,quantity,quantity)
}

fun Context.font(fontId : Int) : Typeface? {
    return ResourcesCompat.getFont(this,fontId)
}
