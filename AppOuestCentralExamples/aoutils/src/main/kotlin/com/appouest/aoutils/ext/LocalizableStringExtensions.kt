package com.appouest.aoutils.ext

import android.content.Context

fun Context.getLocalizableString(key: String?): String {
    if (key == null || key.isEmpty()) {
        return ""
    }
   return try {
        this.resources.getString(this.resources.getIdentifier(key, "string", this.packageName))
    } catch (e: Exception) {
        key
    }
}

fun Context.getLocalizableStringWithId(id: Int): String {
    return try {
        resources.getString(id)
    } catch (e: Exception) {
        "" + id
    }
}