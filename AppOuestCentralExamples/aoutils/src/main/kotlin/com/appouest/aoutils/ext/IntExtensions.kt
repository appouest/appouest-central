package com.appouest.aoutils.ext

import android.content.res.Resources
import android.graphics.Color

/**
 * Property Extension for dp/px
 */
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun Int.adjustAlpha(factor: Float): Int {
    val alpha = Math.round(Color.alpha(this) * factor)
    val red = Color.red(this)
    val green = Color.green(this)
    val blue = Color.blue(this)
    return Color.argb(alpha, red, green, blue)
}

//Duration to string formatted
fun Int.formatDurationTime(): String {
    val formatted: String
    val minutes = this / 60

    if (minutes > 60) {
        val hours = minutes / 60
        val minutesLeft = minutes % 60
        formatted = if (minutesLeft != 0) {
            String.format("%02dh%02dm", hours, minutesLeft)
        } else {
            hours.toString() + "h"
        }
    } else {
        val secondsLeft = this % 60
        formatted = if (secondsLeft != 0) {
            String.format("%02dm%02ds", minutes, secondsLeft)
        } else {
            minutes.toString() + "m"
        }
    }
    return formatted
}

fun Int.formatVideoTime(): String {
    val formatted: String
    val seconds = this / 1000
    val minutes = seconds / 60

    formatted = if (minutes > 60) {
        val hours = minutes / 60
        val minutesLeft = minutes % 60
        String.format("%02d:%02d", hours, minutesLeft)
    } else {
        val secondsLeft = seconds % 60
        String.format("%02d:%02d", minutes, secondsLeft)
    }

    return formatted
}

fun Long.getFormattedFileSize(): String {
    val suffixes = arrayOf("o", "Ko", "Mo", "Go", "To")

    var tmpSize = this.toDouble()
    var i = 0

    while (tmpSize >= 1024) {
        tmpSize /= 1024.0
        i++
    }

    // arrondi à 10^-2
    tmpSize *= 100.0
    tmpSize = (tmpSize + 0.5).toInt().toDouble()
    tmpSize /= 100.0

    return tmpSize.toString() + " " + suffixes[i]
}