package com.appouest.aoutils.utils

import android.graphics.Rect
import android.graphics.RectF

class CGRect {

    var x: Float = 0.toFloat()
    var y: Float = 0.toFloat()

    var width: Float = 0.toFloat()
    var height: Float = 0.toFloat()

    constructor() {
        x = 0f
        y = 0f
        width = 0f
        height = 0f
    }

    constructor(rectF: RectF) {
        x = rectF.left
        y = rectF.top
        width = rectF.width()
        height = rectF.height()
    }

    constructor(center: CGPoint, size: CGSize) : this() {
        width = size.width
        height = size.height
        setCenter(center.x, center.y)
    }

    constructor(rect: Rect) : this(RectF(rect)) {}

    constructor(rect: CGRect) {
        x = rect.x
        y = rect.y
        width = rect.width
        height = rect.height
    }

    constructor(x: Float, y: Float, width: Float, height: Float) {
        this.x = x
        this.y = y
        this.width = width
        this.height = height
    }

    fun setCenter(x: Float, y: Float) {
        this.x = x - width / 2.0f
        this.y = y - height / 2.0f
    }

    fun rectF(): RectF {
        return RectF(x, y, x + width, y + height)
    }

    fun rect(): Rect {
        return Rect(x.toInt(), y.toInt(), (x + width).toInt(), (y + height).toInt())
    }
}