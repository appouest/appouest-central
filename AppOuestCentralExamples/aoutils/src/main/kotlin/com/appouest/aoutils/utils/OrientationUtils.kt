package com.appouest.aoutils.utils

import android.content.Context
import android.provider.Settings

object OrientationUtils {
    private val THRESHOLD = 15

    fun isLandscape(orientation: Int): Boolean {
        return orientation >= 90 - THRESHOLD && orientation <= 90 + THRESHOLD || orientation >= 270 - THRESHOLD && orientation <= 270 + THRESHOLD
    }

    fun isPortrait(orientation: Int): Boolean {
        return orientation >= 360 - THRESHOLD && orientation <= 360 || orientation in 0..THRESHOLD
    }

    fun isOrientationLocked(context: Context): Boolean {
        return android.provider.Settings.System.getInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION, 0) == 0
    }
}