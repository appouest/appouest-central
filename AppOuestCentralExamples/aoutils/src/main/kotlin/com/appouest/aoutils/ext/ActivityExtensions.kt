package com.appouest.aoutils.ext

import android.app.Activity
import android.app.ActivityManager
import android.content.Context

fun Activity.isLastActivity(): Boolean {
    val mngr = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val taskList = mngr.getRunningTasks(10)
    return taskList[0].numActivities == 1 && taskList[0].topActivity.className == this.javaClass.name
}