package com.appouest.aoutils.utils

import java.security.SecureRandom

object RandomUtils {
    internal val AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    internal var rnd = SecureRandom()

    fun randomString(len: Int): String {
        val sb = StringBuilder(len)
        for (i in 0 until len)
            sb.append(AB[rnd.nextInt(AB.length)])
        return sb.toString()
    }
}