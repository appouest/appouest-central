package com.appouest.aoutils.animation

import android.support.annotation.UiThread
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator

@UiThread
fun View.slideInToTop(animated: Boolean) {
    this.visibility = View.VISIBLE
    this.animate().translationY(0f).alpha(1f).setDuration((if (animated) 300 else 0).toLong()).interpolator = DecelerateInterpolator()
}

@UiThread
fun View.fadeOut() {
    fadeOut(500)
}

@UiThread
fun View.fadeIn() {
    fadeIn(500)
}

@UiThread
fun View.fadeIn(duration: Long) {
    this.clearAnimation()
    val anim = AlphaAnimation(this.alpha, 1.0f)
    anim.duration = duration
    this.startAnimation(anim)
}

@UiThread
fun View.fadeOut(duration: Long) {
    this.clearAnimation()
    val anim = AlphaAnimation(this.alpha, 0.0f)
    anim.duration = duration
    this.startAnimation(anim)
}

@UiThread
fun View.startFadeIn(duration: Long = 500) {
    if (this.visibility == View.INVISIBLE) {
        this.visibility = View.VISIBLE
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.duration = duration
        this.startAnimation(fadeIn)
    }
}

@UiThread
fun View.startFadeOut(duration: Long = 500) {
    if (this.visibility == View.VISIBLE) {
        val fadeOut = AlphaAnimation(1f, 0f)
        fadeOut.duration = duration
        this.startAnimation(fadeOut)
        this.visibility = View.INVISIBLE
    }
}