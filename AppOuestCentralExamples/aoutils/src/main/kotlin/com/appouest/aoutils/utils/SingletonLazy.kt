package com.appouest.aoutils.utils

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class SingletonLazy<T : Any>(val initBlock: () -> T, val clazz: Class<T>) {
    operator fun <R> provideDelegate(ref: R, prop: KProperty<*>): ReadOnlyProperty<R, T> = delegate()

    @Suppress("UNCHECKED_CAST")
    private fun <R> delegate(): ReadOnlyProperty<R, T> = object : ReadOnlyProperty<R, T> {
        override fun getValue(thisRef: R, property: KProperty<*>): T {
            val hash = clazz.hashCode()
            val cached = singletonsCache[hash]
            if (cached != null) return cached as T
            return initBlock().apply { singletonsCache[hash] = this }
        }
    }
}

private val singletonsCache = HashMap<Int, Any>()

fun <T> clearSingleton(clazz: Class<T>) : Boolean {
    val hash = clazz.hashCode()

    singletonsCache.remove(hash)
    return true
}

inline fun <reified T : Any> singletonLazy(noinline block: () -> T): SingletonLazy<T>
        = SingletonLazy(block, T::class.java)