package com.appouest.aoutils.ext

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.view.KeyCharacterMap
import android.view.KeyEvent

val Context.screenWidth: Int
    get() = Resources.getSystem().displayMetrics.widthPixels
val Context.screenHeight: Int
    get() = Resources.getSystem().displayMetrics.heightPixels


fun Context.getNavigationBarHeight(): Int {
    val id = resources.getIdentifier(
            "navigation_bar_height", "dimen", "android")
    return if (id > 0) {
        resources.getDimensionPixelSize(id)
    } else 0
}

fun isNavigationBarAvailable(): Boolean {

    if (Build.PRODUCT.startsWith("sdk_google")) {
        return true
    }

    val hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK)
    val hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME)

    return !(hasBackKey && hasHomeKey)
}