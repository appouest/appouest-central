package com.appouest.aoutils.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import kotlin.reflect.KProperty

/**
 * Just use it like this :
 *  var myPref: String by Preference(MyApplication.instance, PREF_ORGA,"")
 */
class Preference<T>(private val context : Context, private val name : String, private val default: T){

    private val prefs: SharedPreferences by lazy {
        context.getSharedPreferences("default", Context.MODE_PRIVATE)
    }

    operator fun getValue(thisRef: Any? , property: KProperty<*>) : T = findPreference(name,default)

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) = putPreference(name,value)

    @Suppress("UNCHECKED_CAST")
    private fun findPreference(name: String, default: T): T = with(prefs){
        val res : Any = when(default){
            is Long -> getLong(name,default)
            is String -> getString(name,default)
            is Int -> getInt(name,default)
            is Boolean -> getBoolean(name,default)
            is Float -> getFloat(name,default)
            else -> throw IllegalArgumentException("this type can't be saved into Prefs")
        }
        res as T
    }

    @SuppressLint("CommitPrefEdits")
    private fun putPreference(name: String, value: T) = with(prefs.edit()){
        when(value){
            is Long -> putLong(name,value)
            is String -> putString(name,value)
            is Int -> putInt(name,value)
            is Boolean -> putBoolean(name,value)
            is Float -> putFloat(name,value)
            else -> throw IllegalArgumentException("this type can't be saved into Prefs")
        }.apply()
    }
}