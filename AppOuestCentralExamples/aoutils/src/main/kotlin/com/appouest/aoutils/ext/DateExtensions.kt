package com.appouest.aoutils.ext

import java.text.DateFormatSymbols
import java.util.*


fun Date.day(): String {

    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar.get(Calendar.DAY_OF_MONTH).toString()
}

var shortMonths = DateFormatSymbols().getShortMonths()

fun Date.month(): String {

    val calendar = Calendar.getInstance()
    calendar.time = this
    return shortMonths.get(calendar.get(Calendar.MONTH))
}
