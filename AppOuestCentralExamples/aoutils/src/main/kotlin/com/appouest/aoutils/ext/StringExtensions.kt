package com.appouest.aoutils.ext

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.Spanned
import android.text.style.TextAppearanceSpan
import com.appouest.aoutils.view.CustomTypefaceSpan
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.regex.Pattern


fun isEmailValid(email: String): Boolean {
    return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
    ).matcher(email).matches()
}

fun String.toCapitalized(lowercaseTail: Boolean = false): String
{
    var value = ""
    if (length > 0) {
        value += substring(0, 1).toUpperCase()
        if (lowercaseTail) {
            if (length > 1) {
                value += substring(1).toLowerCase()
            }
        }
        else {
            if (length > 1) {
                value += substring(1)
            }
        }
    }
    return value
}

fun String.toHtml(): Spanned {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        return Html.fromHtml(this)
    }
}

fun String.toHtmlTrim(): CharSequence {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        return Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY).trim()
    } else {
        @Suppress("DEPRECATION")
        return Html.fromHtml(this).trim()
    }
}

fun String.applyStyle(context: Context, fontPath: String, style: Int? = null, tag: String): SpannableString {

    val font = Typeface.createFromAsset(context.assets, fontPath)

    val startTag = "<" + tag + ">"
    val endTag = "</" + tag + ">"

    val stripedValue = this.replace(startTag, "").replace(endTag, "")
    var styledText = SpannableString(stripedValue)

    var stringCopy = this
    var startLocation = stringCopy.indexOf(startTag)
    var endLocation = stringCopy.indexOf(endTag)


    while (startLocation != -1 && endLocation != -1) {

        val textToStyle = stringCopy.substring(startLocation + startTag.length, endLocation)

        stringCopy = stringCopy.replaceRange(startLocation, endLocation + endTag.length, textToStyle)

        styledText.setSpan(CustomTypefaceSpan("", font), startLocation, startLocation + textToStyle.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)

        if (style != null) {
            styledText.setSpan(TextAppearanceSpan(context, style), startLocation, startLocation + textToStyle.length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
        }

        startLocation = stringCopy.indexOf(startTag)
        endLocation = stringCopy.indexOf(endTag)
    }

    return styledText
}

fun String.countLines(): Int {
    val lines = this.split("\r\n|\r|\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    return lines.size
}

fun String.md5(): String? {
    try {
        val digest = MessageDigest.getInstance("MD5")
        digest.update(this.toByteArray(), 0, this.length)
        return BigInteger(1, digest.digest()).toString(16)
    } catch (e: NoSuchAlgorithmException) {
    }

    return null
}