package com.appouest.aoutils.utils;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.webkit.MimeTypeMap;

import com.appouest.aoutils.ext.IntExtensionsKt;

import java.io.File;
import java.io.FileInputStream;

public class FileUtils {

    public static File getApplicationRootDirectory(Context context) {
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" +
                context.getPackageName());
    }

    public static File getApplicationTmpDirectory(Context context) {
        final File tmp = new File(getApplicationRootDirectory(context), "tmp");
        tmp.mkdirs();
        return tmp;
    }

    public static File createTempFile(Context context) {
        return createTempFile(context, "temp_");
    }

    public static void createNoMediaMarkerFile(File tmpDirectory) {
        File file = new File(tmpDirectory, ".nomedia");
    }

    public static File createTempFile(Context context, String prefix) {
        File applicationTmpDirectory = getApplicationTmpDirectory(context);
        createNoMediaMarkerFile(applicationTmpDirectory);
        return new File(applicationTmpDirectory, prefix + System.currentTimeMillis());
    }

    public static byte[] convertFileToByteArray(File file)
    {
        FileInputStream fileInputStream=null;

        byte[] bFile = new byte[(int) file.length()];

        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();

        } catch (Exception e){
            e.printStackTrace();
        }
        return bFile;
    }

    public static File fromUri(Uri uri) {
        if (uri == null || !"file".equals(uri.getScheme())) {
            return null;
        }
        return new File(uri.getPath());
    }


    public static void empty(File directory) {
        if (!directory.isDirectory()) {
            return;
        }
        delete(directory, false);
    }

    public static void delete(File fileOrDirectory, boolean deleteMe) {
        if (fileOrDirectory.isDirectory()) {
            final File[] children = fileOrDirectory.listFiles();
            if (children == null) {
                return;
            }
            for (File child : children) {
                delete(child);
            }
        }
        if (deleteMe) {
            fileOrDirectory.delete();
        }
    }

    public static void delete(File fileOrDirectory) {
        delete(fileOrDirectory, true);
    }

    public static void deleteImage(Context context, String path) {
        // http://stackoverflow.com/questions/4430888/android-file-delete-leaves-empty-placeholder-in-gallery
        context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.Media.DATA + "=?", new String[]{path});
    }

    public static String getMimeType(String url) {
        String type = "unknown";
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }

        if (type == null) {
            type = "unknown";
        }
        return type;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else
            if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static String getAvailableSpace(){
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        long   free   = (statFs.getAvailableBlocksLong() * (long) statFs.getBlockSizeLong());
        return IntExtensionsKt.getFormattedFileSize(free);
    }
}