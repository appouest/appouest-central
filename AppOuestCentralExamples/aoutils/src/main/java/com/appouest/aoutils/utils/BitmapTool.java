package com.appouest.aoutils.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BitmapTool {

    public static Bitmap scaledBitmapForServer(Bitmap bitmap) {
        int maxSize = 1600;
        if(bitmap.getWidth() > maxSize || bitmap.getHeight() > maxSize) {
            Matrix m = new Matrix();
            if (bitmap.getHeight() > bitmap.getWidth()) {
                int newWidth = maxSize * bitmap.getWidth() / bitmap.getHeight();
                Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, maxSize, false);
                bitmap.recycle();
                return newBitmap;
            }
            else {
                int newHeight = maxSize * bitmap.getHeight() / bitmap.getWidth();
                Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, maxSize, newHeight, false);
                bitmap.recycle();
                return newBitmap;
            }
        } else {
            return bitmap;
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float)height / (float)reqHeight);
            } else {
                inSampleSize = Math.round((float)width / (float)reqWidth);
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromUri(Context context, Uri uri, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri), null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }



    public static File tempFileForBitmap(Context context, Bitmap bitmap, String name)
    {
        try {
            File outputDir = context.getExternalCacheDir(); // context being the Activity pointer
            File outputFile = File.createTempFile(name, ".jpg", outputDir);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
            byte[] bitmapdata = bos.toByteArray();

            FileOutputStream fos = new FileOutputStream(outputFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            return outputFile;
        }
        catch (Exception e) {

        }
        return null;
    }

    public static Bitmap rotateBitmapIfNeeded(Context context, Bitmap bitmap, String imageUri)
    {
        int orientation = getImageRotation(context, Uri.parse(imageUri));
        if (orientation != 0) {
            bitmap = rotateImage(bitmap, orientation);
        }
        return bitmap;
    }

    public static boolean shouldRotatePicture(Context context, String filePath)
    {
        int orientation = getImageRotation(context, Uri.parse(filePath));
        boolean result = orientation != 0;
        return result;
    }

    public static int getImageRotation(Context context, Uri imageUri) {
        try {
            ExifInterface exif = new ExifInterface(imageUri.getPath());
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);

            if (rotation == ExifInterface.ORIENTATION_UNDEFINED)
                return getRotationFromMediaStore(context, imageUri);
            else return exifToDegrees(rotation);
        } catch (IOException e) {
            return 0;
        }
    }

    public static int getRotationFromMediaStore(Context context, Uri imageUri) {
        String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = context.getContentResolver().query(imageUri, columns, null, null, null);
        if (cursor == null) return 0;

        cursor.moveToFirst();

        int orientationColumnIndex = cursor.getColumnIndex(columns[1]);
        return cursor.getInt(orientationColumnIndex);
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        } else {
            return 0;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        source.recycle();
        return retVal;
    }
}