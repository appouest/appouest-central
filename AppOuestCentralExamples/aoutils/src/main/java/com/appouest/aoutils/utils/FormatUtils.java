package com.appouest.aoutils.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;

public class FormatUtils {
    private static NumberFormat CURRENCY_FORMATTER = getCurrencyFormatter();

    private static NumberFormat getCurrencyFormatter() {
        final NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setCurrency(Currency.getInstance("EUR"));
        return numberFormat;
    }

    public static String formatPrice(BigDecimal price) {
        return CURRENCY_FORMATTER.format(price) + "€";
    }
}
